package org.linkedopenactors.rdfpub.keycloak.setup.local;

import javax.ws.rs.ProcessingException;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.policy.TimeoutRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication	
@Slf4j
public class SpringApp implements CommandLineRunner, ApplicationContextAware {

//	public static final String KEYCLOAK_SERVER = "localhost:8080";
	public static final String LOA_REALM = "LOA";
	
	@Autowired
	private UserImport userImport;
	
	@Value("${keycloak.server-url}")
	private String keycloakServerUrl;
	
	private ApplicationContext applicationContext;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringApp.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			log.info(this.getClass().getSimpleName() + " starting...");
			waitForKeycloak(KeycloakBuilder.builder()
				    .serverUrl(keycloakServerUrl)//"http://" + KEYCLOAK_SERVER + "/auth")
				    .grantType(OAuth2Constants.PASSWORD)
				    .realm("master")
				    .clientId("admin-cli")
				    .username("admin")
				    .password("admin")
				    .build(), LOA_REALM);
			userImport.doImport(keycloakServerUrl);
		} finally {
			if(applicationContext!=null) {
				log.info("shuting down " + SpringApp.class.getSimpleName());
				SpringApplication.exit(applicationContext);
			}
		}		
	}
	
	private void waitForKeycloak(Keycloak keycloak, String realName) {
		RetryTemplate template = RetryTemplate.builder()
	      .fixedBackoff(2000)
	      .withinMillis(60000L)
	      .build();
		
		TimeoutRetryPolicy policy = new TimeoutRetryPolicy();
		policy.setTimeout(60000L);

		template.setRetryPolicy(policy);
		
		template.execute(new RetryCallback<RealmResource,ProcessingException>() {
		    public RealmResource doWithRetry(RetryContext context) {
		    	try {
					log.debug("waiting for keykcloak realm '"+realName+"' ...");
					RealmResource realmResource = keycloak.realm(realName);
					realmResource.clients().findAll();
					log.debug("realm loaded");
					return realmResource;
				} catch (Exception e) {
//					log.error("problems accessing keycloak: ", e);
					throw e;
				}
		    }
		});		
	}
	
	@Bean
	public WebClient.Builder webClientBuilder() {
		return WebClient.builder();
	}
	
	@Bean
	public WebClient getWebClient() {
		return webClientBuilder().build();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.info("applicationContext initialized");
		this.applicationContext = applicationContext;		
	}	
}

