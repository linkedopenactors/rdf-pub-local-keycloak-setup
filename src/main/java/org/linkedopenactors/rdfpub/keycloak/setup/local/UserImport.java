package org.linkedopenactors.rdfpub.keycloak.setup.local;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserImport {

	private static final String RDF_PUB_ADMIN_USERNAME = "rdf-pub-admin";
	private static final String RDF_PUB_ADMIN_PASSWORD = "admin";
	private static final String RDF_PUB_ADMIN_MAIL = "rdf-pub-admin@localhost";

	private static final String KVM_ADAPTER_ADMIN_USERNAME = "kvmAdapter";
	private static final String KVM_ADAPTER_ADMIN_PASSWORD = "kvmAdapter";
	private static final String KVM_ADAPTER_ADMIN_MAIL = "kvmAdapter@localhost";

	private String rdfPubAdminUserId;

	public void doImport(String keycloakServerUrl) {
		Keycloak keycloak = KeycloakBuilder.builder()
			    .serverUrl(keycloakServerUrl)
			    .grantType(OAuth2Constants.PASSWORD)
			    .realm("master")
			    .clientId("admin-cli")
			    .username("admin")
			    .password("admin")
			    .build();
		keycloak.tokenManager().getAccessToken();
		RealmResource loaRealm = keycloak.realm(SpringApp.LOA_REALM);
		Optional<UserRepresentation> rdfPubAdminOptional = findUser(loaRealm, RDF_PUB_ADMIN_MAIL);

		UsersResource loaUsers = loaRealm.users();
		if(rdfPubAdminOptional.isEmpty()) {
			UserRepresentation rdfPubAdminToCreate = getUser(RDF_PUB_ADMIN_USERNAME, RDF_PUB_ADMIN_MAIL, RDF_PUB_ADMIN_PASSWORD, Collections.emptyMap(), Collections.emptyList());
			rdfPubAdminUserId = CreatedResponseUtil.getCreatedId(loaUsers.create(rdfPubAdminToCreate));
			log.info("### created: " + RDF_PUB_ADMIN_USERNAME + " ("+rdfPubAdminUserId+") at: " + keycloakServerUrl);
			UserRepresentation kvmAdapterToCreate = getUser(KVM_ADAPTER_ADMIN_USERNAME, KVM_ADAPTER_ADMIN_MAIL, KVM_ADAPTER_ADMIN_PASSWORD, Collections.emptyMap(), Collections.emptyList());
			String id = CreatedResponseUtil.getCreatedId(loaUsers.create(kvmAdapterToCreate));
			log.info("### created: " + KVM_ADAPTER_ADMIN_USERNAME + " ("+id+") at: " + keycloakServerUrl);
		}

		// ADD role 
		ClientRepresentation reamManagementClient = getRealmManegementClient(keycloak);		
		
		RoleRepresentation viewUsersRole =  loaRealm
				.clients()
				.get(reamManagementClient.getId())
				.roles()
				.list()
				.stream()
				.filter(role->role.getName().equals("view-users"))
				.findFirst()
				.orElseThrow();

		loaUsers.get(loaUsers.get(rdfPubAdminUserId).toRepresentation().getId())
			.roles()
			.clientLevel(reamManagementClient.getId())
			.add(List.of(viewUsersRole));
	}

	private ClientRepresentation getRealmManegementClient(Keycloak keycloak) {
		List<ClientRepresentation> clients = keycloak.realm(SpringApp.LOA_REALM).clients().findAll().stream()
				.filter(client -> client.getClientId().equals("realm-management")).collect(Collectors.toList());
		if(clients.size()!=1) {
			throw new RuntimeException("not exact one realm-management");
		}		
		ClientRepresentation reamManagementClient = clients.get(0);
		return reamManagementClient;
	}

	private Optional<UserRepresentation> findUser(RealmResource realmResource, String email) {
		String existingUseres = realmResource.users().list().stream().map(UserRepresentation::getEmail).collect(Collectors.joining(","));
		Optional<UserRepresentation> result = realmResource.users().list().stream()
				.filter(gr->email.equals(gr.getEmail()))
				.findFirst();
		if(result.isEmpty()) {
			log.info(email + " not existent in " + existingUseres);
		} else {
			log.info(email + " EXISTS in " + existingUseres);
		}
		return result;
	}

	private UserRepresentation getUser(String userName, String email, String password, Map<String, List<String>> attributes, List<String> groups) {
		UserRepresentation userRepresentation = new UserRepresentation();
		userRepresentation.setUsername(userName);
		userRepresentation.setEmail(email);
		userRepresentation.setEnabled(true);
		userRepresentation.setEmailVerified(true);
		userRepresentation.setAttributes(attributes);
		if(!CollectionUtils.isEmpty(groups)) {
			userRepresentation.setGroups(groups);
		}
		
		CredentialRepresentation cr = new CredentialRepresentation();
		cr.setType(CredentialRepresentation.PASSWORD);
		cr.setValue(password);
		cr.setTemporary(false);
		userRepresentation.setCredentials(List.of(cr));
		
		return userRepresentation;
	}
}
